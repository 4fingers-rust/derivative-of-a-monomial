# Derivative of a Monomial

# What is does?

Suppose we had a monomial in the form of `f(x) = a * x^n`, you will input coefficient and power of x, then program will find the deravative of the given monomial.

# Test the program

First of all, you must install Rust first. After that, you have to clone this repository to your local hard drive.

Run:

`cd derivative-of-a-monomial && cargo run`

Have fun!