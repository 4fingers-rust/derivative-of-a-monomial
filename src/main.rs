use std::io;
use std::fmt;

struct Monomial{
    coefficient: f64,
    power_of_variable: f64,
}

impl Monomial {
    fn new(coefficient: f64, power_of_variable: f64) -> Monomial {
        Monomial {
            coefficient: coefficient,
            power_of_variable: power_of_variable
        }
    }
}

impl fmt::Display for Monomial {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} * x^{}", self.coefficient, self.power_of_variable)
    }
}

// Standard input macro
macro_rules! input {
    ($value:ident) => {
        let mut $value = String::new();
        match io::stdin().read_line(&mut $value) {
            Ok(_n) => {},
            Err(e) => println!("Error: {}", e)
        }
    };
}

fn str_to_f64(str: String) -> f64 {
    str.trim().parse().unwrap()
}

// Borrow Monomial data to avoid ownership error which will happen in <fn main()> later
fn derivative_of(m: &Monomial) -> Monomial {
    Monomial {
        coefficient: &m.coefficient * &m.power_of_variable,
        power_of_variable: &m.power_of_variable - 1.0
    }
}

fn main() {
    println!("A simple command line program to find the derivative of a monomial in the form of f(x) = a * x^n");
    println!("Please input COEFFICIENT: ");
    input!(inputed_coefficient);
    println!("Please input POWER OF VARIABLE: ");
    input!(inputed_power_of_variable);

    let monomial = Monomial::new(str_to_f64(inputed_coefficient), str_to_f64(inputed_power_of_variable));
    // That's why I need to borrow
    let new_monomial = derivative_of(&monomial);

    println!("Derivative of {} is {}", monomial, new_monomial);
}
